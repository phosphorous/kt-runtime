KtRuntime
[![Latest version](https://img.shields.io/maven-metadata/v?metadataUrl=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F39310754%2Fpackages%2Fmaven%2Fio%2Fgitlab%2Faecsocket%2Fkt-runtime-paper%2Fmaven-metadata.xml)](https://gitlab.com/phosphorous/kt-runtime/-/packages/12085259)
[![Pipeline status](https://img.shields.io/gitlab/pipeline-status/phosphorous/kt-runtime?branch=main)](https://gitlab.com/phosphorous/kt-runtime/-/pipelines/latest)
===

Kotlin runtime support for the Paper server platform.
The project version corresponds to the same Kotlin version packaged.

Features
---

* 1.13+ support
* `kotlin-stdlib`
* `kotlin-reflect`
* `kotlinx-coroutines-core`

### [Quickstart and documentation](https://phosphorous.gitlab.io/kt-runtime)

Downloads for v1.8.0
---

### [Paper](https://gitlab.com/api/v4/projects/39310754/jobs/artifacts/main/raw/paper/build/libs/kt-runtime-paper-1.8.0-all.jar?job=build)
