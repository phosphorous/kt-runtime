plugins {
    id("com.github.johnrengelman.shadow")
}

val minecraft = libs.versions.minecraft.get()

repositories {
    maven("https://repo.papermc.io/repository/maven-public/")
}

dependencies {
    compileOnly("com.destroystokyo.paper", "paper-api", "$minecraft-R0.1-SNAPSHOT")

    api(libs.kotlinStdlib)
    api(libs.kotlinReflect)
    api(libs.kotlinxCoroutines)
}

tasks {
    shadowJar {
        mergeServiceFiles()
    }

    assemble {
        dependsOn(shadowJar)
    }
}
